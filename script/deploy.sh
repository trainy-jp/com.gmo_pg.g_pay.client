#!/bin/bash -e

readonly MVN_GROUP_ID=com.gmo_pg
readonly MVN_ARTIFACT_ID=g_pay.client
readonly MVN_DESCRIPTION=GMO Payment Gateway Client

readonly MVN_REPOSITORY_ID=maven-3rdparty
readonly MVN_REPOSITORY_URL=https://nexus.development.bookfit.jp/repository/$MVN_REPOSITORY_ID/

readonly SCRIPT_DIR=$(dirname $0)

# 引数チェック(zipのあるパス)
readonly BIN_DIR=$1
if [ $# -ne 1 ]; then
    echo "引数が1以外指定されています。" 1>&2
    exit 1;
fi

echo $BIN_DIR
if [ ! -d $BIN_DIR ]; then
    echo "ディレクトリ $BIN_DIR が存在しません。" 1>&2
    exit 2;
fi

ls $BIN_DIR/*.zip
if [ $? -ne 0 ]; then
    echo "処理できるファイルがありません。" 1>&2
    exit 0;
fi

# ファイルごとに処理
for f in $(find $BIN_DIR -maxdepth 1 -name '*.zip' -print); do
    cd $BIN_DIR
    EXTRACT_DIR=$(basename $f | sed 's/\.[^\.]*$//')
    # バージョン
    MVN_VERSION=$(echo $EXTRACT_DIR | sed 's/ModuleTypeJAVA_//')
    echo "MVN_VERSION = $MVN_VERSION"
    # 本体jarの抽出
    unzip -o $f gpay_client/lib/*.jar -d $EXTRACT_DIR
    JAR_PATH=$(find $BIN_DIR/$EXTRACT_DIR/gpay_client/lib -maxdepth 1 -name '*.jar' -print)
    mv $JAR_PATH $BIN_DIR/$EXTRACT_DIR/gpay_client/lib/gpay.client-$MVN_VERSION.jar
    JAR_PATH=$BIN_DIR/$EXTRACT_DIR/gpay_client/lib/gpay.client-$MVN_VERSION.jar
    echo "JAR_PATH = $JAR_PATH"
    # javadocの展開
    unzip -o $f gpay_client/doc/** -d $EXTRACT_DIR
    DOC_PATH=$BIN_DIR/$EXTRACT_DIR/gpay_client/doc
    JAVADOC_PATH=$BIN_DIR/$EXTRACT_DIR/gpay_client/lib/gpay.client-$MVN_VERSION-javadoc.jar
    echo "DOC_PATH = $DOC_PATH"
    echo "JAVADOC_PATH = $JAVADOC_PATH"
    cd $DOC_PATH
    zip -r $JAVADOC_PATH .
    cd $BIN_DIR
    # maven deploy
    mvn deploy:deploy-file -DgroupId=$MVN_GROUP_ID \
        -DartifactId=$MVN_ARTIFACT_ID \
        -Dversion=$MVN_VERSION \
        -Dpackaging=jar \
        -Dfile=$JAR_PATH \
        -Djavadoc=$JAVADOC_PATH \
        -DgeneratePom=true \
        -DrepositoryId=$MVN_REPOSITORY_ID \
        -Durl=$MVN_REPOSITORY_URL \
        -s $SCRIPT_DIR/maven-settings.xml
done