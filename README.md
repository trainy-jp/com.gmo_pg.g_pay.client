# GMO Payment Gateway クライアント用リポジトリ

## 追加ファイル

`bin` ディレクトリに、 `ModuleTypeJAVA_<version>` の名前で格納してください。  
例) `bin/ModuleTypeJAVA_1.33.zip`  
  
※以下、手動時の説明  

## pomへの追加方法

1. `<project>` 配下に以下のリポジトリ設定を追加する  
```xml
<repositories>
    <repository>
    	<id>gmo_pg.gpay_client</id>
		<name>GMO Payment Gateway Library</name>
		<url>https://bitbucket.org/trainy-jp/com.gmo_pg.g_pay.client/raw/master/repository</url>
    </repository>
</repositories>
```  
1. 依存関係を追加する  
```xml
<dependency>
	<groupId>com.gmo_pg</groupId>
	<artifactId>g_pay.client</artifactId>
	<version>1.41</version>
</dependency>
```

## 新バージョン追加方法

### zipファイルの格納

1. bin フォルダに、 `ModuleTypeJAVA_*_**.zip` のファイル名で格納する。

### 一発実施コマンド

```bash
$ cd script
$ ./add.sh -f ../bin/ModuleTypeJAVA_*_**.zip -v *.**
```

### 手動実施方法

#### jarファイルの配置

1. `repository/com/gmo_pg/g_pay.client` にバージョン番号でフォルダを作成する。
例:  
```bash
$ cd repository/com/gmo_pg/g_pay.client
$ mkdir 1.41
```  
1. ダウンロードしたモジュールタイプの、 `gpay_client/lib` にあるjarファイルを作成したフォルダに格納する。  
ファイル名は `g_pay.client-1.41.jar` などのようにする。  
1. `g_pay.client-1.41.pom` を作成する。  
例:  
```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd" xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <modelVersion>4.0.0</modelVersion>
  <groupId>com.gmo_pg</groupId>
  <artifactId>g_pay.client</artifactId>
  <version>1.41</version>
  <description>GMO Payment Gateway Client</description>
</project>
```
バージョン番号 `<version>` タグを書き換え忘れないようにする。  
1. MD5を計算し、 `g_pay.client-1.41.*.md5` を書き換える。（末尾の改行は削除すること）  
例:  
```bash
md5 -q g_pay.client-1.41.jar | tr -d '\n' > g_pay.client-1.41.jar.md5
md5 -q g_pay.client-1.41.pom | tr -d '\n' > g_pay.client-1.41.pom.md5
```  
32バイトであれば正解。
1. SHA1を計算し、 `g_pay.client-1.41.*.sha1` を書き換える。
例:  
```bash
shasum g_pay.client-1.41.jar | cut -c 1-40 | tr -d '\n' > g_pay.client-1.41.jar.sha1
shasum g_pay.client-1.41.pom | cut -c 1-40 | tr -d '\n' > g_pay.client-1.41.pom.sha1
```  
40バイトであれば正解。  

#### javadocファイルの生成

1. ダウンロードしたモジュールタイプの、 `gpay_client/doc` に移動する。
1. 配下のファイルをすべてzipに圧縮する。  
```bash
$ zip -r g_pay.client-1.41-javadoc.jar .
```
1. できた `g_pay.client-1.41-javadoc.jar` ファイルを `repository/com/gmo_pg/g_pay.client/<バージョン番号>` に移動する。  
1. MD5とSHA1を計算する。（方法はjarと同じ）

#### maven-metadata-local.xmlの編集

1. `repository/com/gmo_pg/g_pay.client` にある `maven-metadata-local.xml` を開く。
1. `<versioning>` 配下にある `<release>` のテキストを最新バージョンにする。
1. `<versions>` 配下に新しいバージョンの `<version>` を追加する。  
例:  
```xml
<?xml version="1.0" encoding="UTF-8"?>
<metadata>
  <groupId>com.gmo_pg</groupId>
  <artifactId>g_pay.client</artifactId>
  <versioning>
    <release>1.41</release>
    <versions>
      <version>1.41</version>
      <version>1.36</version>
      <version>1.33</version>
    </versions>
    <lastUpdated>20170206215016</lastUpdated>
  </versioning>
</metadata>
```
1. MD5を計算し、 `maven-metadata-local.md5` を書き換える。
1. SHA1を計算し、 `maven-metadata-local.sha1` を書き換える。
